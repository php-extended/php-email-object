# php-extended/php-email-object
A library that implements the php-email-interface library.

![coverage](https://gitlab.com/php-extended/php-email-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-email-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-email-object ^8`


## Basic Usage

This library provides the additional Email class to the psr-7, but follows its rules.
More information is available at http://www.php-fig.org/psr/psr-7/](http://www.php-fig.org/psr/psr-7/).


## License

MIT (See [license file](LICENSE)).
