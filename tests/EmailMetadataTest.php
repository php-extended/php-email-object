<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\Email\EmailAddress;
use PhpExtended\Email\EmailAddressList;
use PhpExtended\Email\EmailMetadata;
use PhpExtended\Email\Mailbox;
use PhpExtended\Email\MailboxList;
use PHPUnit\Framework\TestCase;

/**
 * EmailMetadataTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Email\EmailMetadata
 *
 * @internal
 *
 * @small
 */
class EmailMetadataTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var EmailMetadata
	 */
	protected EmailMetadata $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new EmailMetadata(
			'provider',
			'identifier',
			'subject',
			new MailboxList(),
			new EmailAddressList(),
			DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01'),
			new Mailbox(new EmailAddress('local', new Domain(['domain', 'com']))),
			new EmailAddressList(),
			new EmailAddressList(),
			new EmailAddressList(),
			DateTimeImmutable::createFromFormat('!Y-m-d', '2002-02-02'),
		);
	}
	
}
