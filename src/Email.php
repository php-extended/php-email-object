<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\HttpMessage\Message;
use PhpExtended\Parser\ParseThrowable;

/**
 * Email class file.
 * 
 * This class is a simple implementation of the EmailInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class Email extends Message implements EmailInterface
{
	
	/**
	 * The name of the provider for this message.
	 * 
	 * @var string
	 */
	protected ?string $_providerName = null;
	
	/**
	 * The identifier of the message.
	 * 
	 * @var ?string
	 */
	protected ?string $_identifier = null;
	
	/**
	 * The origination date.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_origDate = null;
	
	/**
	 * The from value.
	 * 
	 * @var ?MailboxListInterface
	 */
	protected ?MailboxListInterface $_from = null;
	
	/**
	 * The sender value.
	 * 
	 * @var ?MailboxInterface
	 */
	protected ?MailboxInterface $_sender = null;
	
	/**
	 * The reply to value.
	 * 
	 * @var ?EmailAddressListInterface
	 */
	protected ?EmailAddressListInterface $_replyTo = null;
	
	/**
	 * The to value.
	 * 
	 * @var ?EmailAddressListInterface
	 */
	protected ?EmailAddressListInterface $_dest = null;
	
	/**
	 * The cc value.
	 * 
	 * @var ?EmailAddressListInterface
	 */
	protected ?EmailAddressListInterface $_copy = null;
	
	/**
	 * The bcc value.
	 * 
	 * @var ?EmailAddressListInterface
	 */
	protected ?EmailAddressListInterface $_bcc = null;
	
	/**
	 * The reception date value.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_receptionDate = null;
	
	/**
	 * The subject of the message.
	 * 
	 * @var ?string
	 */
	protected ?string $_subject = null;
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getProviderName()
	 */
	public function getProviderName() : string
	{
		return $this->_providerName ?? '(unknown)';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withProviderName()
	 * @return static
	 */
	public function withProviderName(string $providerName) : EmailInterface
	{
		$newobj = clone $this;
		$newobj->_providerName = $providerName;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getIdentifier()
	 */
	public function getIdentifier() : string
	{
		if(null === $this->_identifier)
		{
			return \spl_object_hash($this);
		}
		
		return $this->_identifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withIdentifier()
	 * @return static
	 */
	public function withIdentifier(string $identifier) : EmailInterface
	{
		$newobj = clone $this;
		$newobj->_identifier = $identifier;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getOrigDate()
	 */
	public function getOrigDate() : ?DateTimeInterface
	{
		return $this->_origDate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withOrigDate()
	 * @return static
	 */
	public function withOrigDate(DateTimeInterface $datetime) : EmailInterface
	{
		$newobj = clone $this;
		$newobj->_origDate = $datetime;
		
		return $newobj;
	}
	
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getFrom()
	 */
	public function getFrom() : MailboxListInterface
	{
		if(null === $this->_from)
		{
			return new MailboxList();
		}
		
		return $this->_from;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withFrom()
	 * @return static
	 */
	public function withFrom(MailboxListInterface $list) : EmailInterface
	{
		$newobj = clone $this;
		$newobj->_from = $list;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getSender()
	 */
	public function getSender() : ?MailboxInterface
	{
		if(null === $this->_sender)
		{
			foreach($this->getFrom() as $mailbox)
			{
				return $mailbox;
			}
		}
		
		return $this->_sender;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withSender()
	 * @return static
	 */
	public function withSender(MailboxInterface $mailbox) : EmailInterface
	{
		$newobj = clone $this;
		$newobj->_sender = $mailbox;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getReplyTo()
	 */
	public function getReplyTo() : EmailAddressListInterface
	{
		if(null === $this->_replyTo)
		{
			return $this->getFrom()->collectEmailAddresses();
		}
		
		return $this->_replyTo;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withReplyTo()
	 * @return static
	 */
	public function withReplyTo(EmailAddressListInterface $list) : EmailInterface
	{
		$newobj = clone $this;
		$newobj->_replyTo = $list;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getTo()
	 */
	public function getTo() : EmailAddressListInterface
	{
		if(null === $this->_dest)
		{
			return new EmailAddressList();
		}
		
		return $this->_dest;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withTo()
	 * @return static
	 */
	public function withTo(EmailAddressListInterface $list) : EmailInterface
	{
		$newobj = clone $this;
		$newobj->_dest = $list;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getCc()
	 */
	public function getCc() : EmailAddressListInterface
	{
		if(null === $this->_copy)
		{
			return new EmailAddressList();
		}
		
		return $this->_copy;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withCc()
	 * @return static
	 */
	public function withCc(EmailAddressListInterface $list) : EmailInterface
	{
		$newobj = clone $this;
		$newobj->_copy = $list;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getBcc()
	 */
	public function getBcc() : EmailAddressListInterface
	{
		if(null === $this->_bcc)
		{
			return new EmailAddressList();
		}
		
		return $this->_bcc;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withBcc()
	 * @return static
	 */
	public function withBcc(EmailAddressListInterface $list) : EmailInterface
	{
		$newobj = clone $this;
		$newobj->_bcc = $list;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getReceptionDate()
	 */
	public function getReceptionDate() : ?DateTimeInterface
	{
		return $this->_receptionDate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withReceptionDate()
	 * @return static
	 */
	public function withReceptionDate(DateTimeInterface $received) : EmailInterface
	{
		$newobj = clone $this;
		$newobj->_receptionDate = $received;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getSubject()
	 */
	public function getSubject() : string
	{
		if(null === $this->_subject)
		{
			return '(no subject)';
		}
		
		return $this->_subject;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withSubject()
	 * @return static
	 */
	public function withSubject(string $subject) : EmailInterface
	{
		$newobj = clone $this;
		$newobj->_subject = $subject;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\HttpMessage\Message::hasHeader()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function hasHeader(string $name) : bool
	{
		$lname = \mb_strtolower($name);
		
		switch($lname)
		{
			case 'orig-date':
				return isset($this->_origDate);
			
			case 'from':
			case 'sender':
			case 'reply-to':
				return isset($this->_from);
			
			case 'to':
				return isset($this->_dest);
			
			case 'cc':
				return isset($this->_copy);
			
			case 'bcc':
				return isset($this->_bcc);
			
			case 'date':
				return isset($this->_receptionDate);
			
			case 'subject':
				return isset($this->_subject);
			
			default:
				return parent::hasHeader($name);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\HttpMessage\Message::getHeader()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function getHeader(string $name) : array
	{
		$lname = \mb_strtolower($name);
		
		switch($lname)
		{
			case 'orig-date':
				$origDate = $this->getOrigDate();
				if(null === $origDate)
				{
					return [];
				}
				
				return $this->getArrayElement($origDate->format('D, d M Y H:i:s \\G\\M\\T'));
			
			case 'from':
				return $this->getArrayElement($this->getFrom()->getCanonicalRepresentation());
			
			case 'sender':
				$sender = $this->getSender();
				if(null === $sender)
				{
					return [];
				}
				
				return $this->getArrayElement($sender->getCanonicalRepresentation());
			
			case 'reply-to':
				return $this->getArrayElement($this->getReplyTo()->getCanonicalRepresentation());
			
			case 'to':
				return $this->getArrayElement($this->getTo()->getCanonicalRepresentation());
			
			case 'cc':
				return $this->getArrayElement($this->getCc()->getCanonicalRepresentation());
			
			case 'bcc':
				return $this->getArrayElement($this->getBcc()->getCanonicalRepresentation());
			
			case 'date':
				$receptionDate = $this->getReceptionDate();
				if(null === $receptionDate)
				{
					return [];
				}
				
				return $this->getArrayElement($receptionDate->format('D, d M Y H:i:s \\G\\M\\T'));
			
			case 'subject':
				return $this->getArrayElement($this->getSubject());
			
			default:
				return parent::getHeader($name);
		}
	}
	
	/**
	 * Gets an array containing the given string if it is not empty.
	 * 
	 * @param ?string $string
	 * @return array<integer, string>
	 */
	public function getArrayElement(?string $string) : array
	{
		if(null === $string || '' === $string)
		{
			return [];
		}
		
		return [$string];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\HttpMessage\Message::getHeaderLine()
	 */
	public function getHeaderLine(string $name) : string
	{
		return \implode(', ', $this->getHeader($name));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\HttpMessage\Message::withHeader()
	 * @param string|array<int|string, string> $value
	 * @return static
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function withHeader(string $name, $value) : EmailInterface
	{
		$lname = \mb_strtolower($name);
		$strvalue = '';
		if(\is_string($value))
		{
			$strvalue = $value;
		}
		if(\is_array($value))
		{
			$strvalue = \implode(' ', $value);
		}
		
		$mblparser = new MailboxListParser();
		$mbparser = new MailboxParser();
		$ealparser = new EmailAddressListParser();
		
		switch($lname)
		{
			case 'orig-date':
				$dti = DateTimeImmutable::createFromFormat('D, d M Y H:i:s \\G\\M\\T', $strvalue);
				if(false === $dti)
				{
					return $this;
				}
				
				return $this->withOrigDate($dti);
			
			case 'from':
				return $this->withFrom($mblparser->parse($strvalue));
			
			case 'sender':
				return $this->withSender($mbparser->parse($strvalue));
			
			case 'reply-to':
				return $this->withReplyTo($ealparser->parse($strvalue));
			
			case 'to':
				return $this->withTo($ealparser->parse($strvalue));
			
			case 'cc':
				return $this->withCc($ealparser->parse($strvalue));
			
			case 'bcc':
				return $this->withBcc($ealparser->parse($strvalue));
			
			case 'date':
				$dti = DateTimeImmutable::createFromFormat('D, d M Y H:i:s \\G\\M\\T', $strvalue);
				if(false === $dti)
				{
					return $this;
				}
				
				return $this->withReceptionDate($dti);
			
			case 'subject':
				return $this->withSubject($strvalue);
			
			default:
				/** @psalm-suppress LessSpecificReturnStatement */
				return parent::withHeader($name, $value);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\HttpMessage\Message::withAddedHeader()
	 * @param string|array<int|string, string> $value
	 * @return static
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function withAddedHeader(string $name, $value) : EmailInterface
	{
		$lname = \mb_strtolower($name);
		$strvalue = '';
		if(\is_string($value))
		{
			$strvalue = $value;
		}
		if(\is_array($value))
		{
			$strvalue = \implode(' ', $value);
		}
		
		$mblparser = new MailboxListParser();
		$mbparser = new MailboxParser();
		$ealparser = new EmailAddressListParser();
		
		switch($lname)
		{
			case 'orig-date':
				$dti = DateTimeImmutable::createFromFormat('D, d M Y H:i:s \\G\\M\\T', $strvalue);
				if(false === $dti)
				{
					return $this;
				}
				
				return $this->withOrigDate($dti);
			
			case 'from':
				$mailboxList = $mblparser->parse($strvalue);
				if(null !== $this->_from)
				{
					$mailboxList = $this->getFrom()->withMailboxList($mailboxList);
				}
				
				return $this->withFrom($mailboxList);
			
			case 'sender':
				return $this->withSender($mbparser->parse($strvalue));
			
			case 'reply-to':
				$addressList = $ealparser->parse($strvalue);
				if(null !== $this->_replyTo)
				{
					$addressList = $this->getReplyTo()->withEmailAddressList($addressList);
				}
				
				return $this->withReplyTo($addressList);
			
			case 'to':
				$addressList = $ealparser->parse($strvalue);
				if(null !== $this->_dest)
				{
					$addressList = $this->getTo()->withEmailAddressList($addressList);
				}
				
				return $this->withTo($addressList);
			
			case 'cc':
				$addressList = $ealparser->parse($strvalue);
				if(null !== $this->_copy)
				{
					$addressList = $this->getCc()->withEmailAddressList($addressList);
				}
				
				return $this->withCc($addressList);
			
			case 'bcc':
				$addressList = $ealparser->parse($strvalue);
				if(null !== $this->_copy)
				{
					$addressList = $this->getBcc()->withEmailAddressList($addressList);
				}
				
				return $this->withBcc($addressList);
			
			case 'date':
				$dti = DateTimeImmutable::createFromFormat('D, d M Y H:i:s \\G\\M\\T', $strvalue);
				if(false === $dti)
				{
					return $this;
				}
				
				return $this->withOrigDate($dti);
			
			case 'subject':
				return $this->withSubject($strvalue);
			
			default:
				/** @psalm-suppress LessSpecificReturnStatement */
				return parent::withAddedHeader($name, $value);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\HttpMessage\Message::withoutHeader()
	 * @return static
	 * @psalm-suppress MoreSpecificReturnType
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function withoutHeader(string $name) : EmailInterface
	{
		$lname = \mb_strtolower($name);
		
		switch($lname)
		{
			case 'orig-date':
				$newobj = clone $this;
				$newobj->_origDate = null;
				
				return $newobj;
			
			case 'from':
				$newobj = clone $this;
				$newobj->_from = null;
				
				return $newobj;
			
			case 'sender':
				$newobj = clone $this;
				$newobj->_sender = null;
				
				return $newobj;
			
			case 'reply-to':
				$newobj = clone $this;
				$newobj->_replyTo = null;
				
				return $newobj;
			
			case 'to':
				$newobj = clone $this;
				$newobj->_dest = null;
				
				return $newobj;
			
			case 'cc':
				$newobj = clone $this;
				$newobj->_copy = null;
				
				return $newobj;
			
			case 'bcc':
				$newobj = clone $this;
				$newobj->_bcc = null;
				
				return $newobj;
			
			case 'date':
				$newobj = clone $this;
				$newobj->_receptionDate = null;
				
				return $newobj;
			
			case 'subject':
				$newobj = clone $this;
				$newobj->_subject = null;
				
				return $newobj;
			
			default:
				/** @psalm-suppress LessSpecificReturnStatement */
				return parent::withoutHeader($name);
		}
	}
	
}
