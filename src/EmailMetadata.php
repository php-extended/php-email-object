<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use DateTimeImmutable;
use DateTimeInterface;

/**
 * EmailMetadata class file.
 * 
 * This class is a simple implementation of the EmailMetadataInterface.
 * 
 * @author Anastaszor
 */
class EmailMetadata implements EmailMetadataInterface
{
	
	/**
	 * The name of the provider for this message.
	 * 
	 * @var string
	 */
	protected string $_providerName;
	
	/**
	 * The identifier of the message.
	 * 
	 * @var string
	 */
	protected string $_identifier;
	
	/**
	 * The orig date of the message.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_origDate = null;
	
	/**
	 * The origin of the message.
	 * 
	 * @var MailboxListInterface
	 */
	protected MailboxListInterface $_from;
	
	/**
	 * The sender of the message.
	 * 
	 * @var ?MailboxInterface
	 */
	protected ?MailboxInterface $_sender = null;
	
	/**
	 * Where to redirect the response to this message.
	 * 
	 * @var ?EmailAddressListInterface
	 */
	protected ?EmailAddressListInterface $_replyTo = null;
	
	/**
	 * The receiver of the message.
	 * 
	 * @var EmailAddressListInterface
	 */
	protected EmailAddressListInterface $_dest;
	
	/**
	 * The carbon copies of this message.
	 * 
	 * @var ?EmailAddressListInterface
	 */
	protected ?EmailAddressListInterface $_copy = null;
	
	/**
	 * The black carbon copies of this message.
	 * 
	 * @var ?EmailAddressListInterface
	 */
	protected ?EmailAddressListInterface $_bcc = null;
	
	/**
	 * The date of reception of the message.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_receptionDate;
	
	/**
	 * The subject of the message.
	 * 
	 * @var string
	 */
	protected string $_subject;
	
	/**
	 * Builds a new EmailMetadata with the given parameters.
	 * 
	 * @param string $providerName
	 * @param string $identifier
	 * @param string $subject
	 * @param MailboxListInterface $from
	 * @param EmailAddressListInterface $dest
	 * @param DateTimeInterface $origDate
	 * @param MailboxInterface $sender
	 * @param EmailAddressListInterface $replyTo
	 * @param EmailAddressListInterface $copy
	 * @param EmailAddressListInterface $bcc
	 * @param DateTimeInterface $receptionDate
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(
		string $providerName,
		string $identifier,
		string $subject,
		MailboxListInterface $from,
		EmailAddressListInterface $dest,
		?DateTimeInterface $origDate = null,
		?MailboxInterface $sender = null,
		?EmailAddressListInterface $replyTo = null,
		?EmailAddressListInterface $copy = null,
		?EmailAddressListInterface $bcc = null,
		?DateTimeInterface $receptionDate = null
	) {
		$this->_providerName = $providerName;
		$this->_identifier = $identifier;
		$this->_origDate = $origDate;
		$this->_subject = $subject;
		$this->_from = $from;
		$this->_dest = $dest;
		$this->_sender = $sender;
		$this->_replyTo = $replyTo;
		$this->_copy = $copy;
		$this->_bcc = $bcc;
		if(null === $receptionDate)
		{
			$receptionDate = new DateTimeImmutable();
		}
		$this->_receptionDate = $receptionDate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getProviderName()
	 */
	public function getProviderName() : string
	{
		return $this->_providerName;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withProviderName()
	 * @return static
	 */
	public function withProviderName(string $providerName) : EmailMetadataInterface
	{
		$newobj = clone $this;
		$newobj->_providerName = $providerName;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getIdentifier()
	 */
	public function getIdentifier() : string
	{
		return $this->_identifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withIdentifier()
	 * @return static
	 */
	public function withIdentifier(string $identifier) : EmailMetadataInterface
	{
		$newobj = clone $this;
		$newobj->_identifier = $identifier;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getOrigDate()
	 */
	public function getOrigDate() : ?DateTimeInterface
	{
		return $this->_origDate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withOrigDate()
	 * @return static
	 */
	public function withOrigDate(DateTimeInterface $datetime) : EmailMetadataInterface
	{
		$newobj = clone $this;
		$newobj->_origDate = $datetime;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getFrom()
	 */
	public function getFrom() : MailboxListInterface
	{
		return $this->_from;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withFrom()
	 * @return static
	 */
	public function withFrom(MailboxListInterface $list) : EmailMetadataInterface
	{
		$newobj = clone $this;
		$newobj->_from = $list;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getSender()
	 */
	public function getSender() : ?MailboxInterface
	{
		if(null === $this->_sender)
		{
			foreach($this->_from as $email)
			{
				return $email;
			}
		}
		
		return $this->_sender;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withSender()
	 * @return static
	 */
	public function withSender(MailboxInterface $mailbox) : EmailMetadataInterface
	{
		$newobj = clone $this;
		$newobj->_sender = $mailbox;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getReplyTo()
	 */
	public function getReplyTo() : EmailAddressListInterface
	{
		if(null === $this->_replyTo)
		{
			return $this->_from->collectEmailAddresses();
		}
		
		return $this->_replyTo;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withReplyTo()
	 * @return static
	 */
	public function withReplyTo(EmailAddressListInterface $list) : EmailMetadataInterface
	{
		$newobj = clone $this;
		$newobj->_replyTo = $list;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getTo()
	 */
	public function getTo() : EmailAddressListInterface
	{
		return $this->_dest;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withTo()
	 * @return static
	 */
	public function withTo(EmailAddressListInterface $list) : EmailMetadataInterface
	{
		$newobj = clone $this;
		$newobj->_dest = $list;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getCc()
	 */
	public function getCc() : EmailAddressListInterface
	{
		if(null === $this->_copy)
		{
			return new EmailAddressList();
		}
		
		return $this->_copy;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withCc()
	 * @return static
	 */
	public function withCc(EmailAddressListInterface $list) : EmailMetadataInterface
	{
		$newobj = clone $this;
		$newobj->_copy = $list;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getBcc()
	 */
	public function getBcc() : EmailAddressListInterface
	{
		if(null === $this->_bcc)
		{
			return new EmailAddressList();
		}
		
		return $this->_bcc;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withBcc()
	 * @return static
	 */
	public function withBcc(EmailAddressListInterface $list) : EmailMetadataInterface
	{
		$newobj = clone $this;
		$newobj->_bcc = $list;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getReceptionDate()
	 */
	public function getReceptionDate() : DateTimeInterface
	{
		return $this->_receptionDate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withReceptionDate()
	 * @return static
	 */
	public function withReceptionDate(DateTimeInterface $received) : EmailMetadataInterface
	{
		$newobj = clone $this;
		$newobj->_receptionDate = $received;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::getSubject()
	 */
	public function getSubject() : string
	{
		return $this->_subject;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailMetadataInterface::withSubject()
	 * @return static
	 */
	public function withSubject(string $subject) : EmailMetadataInterface
	{
		$newobj = clone $this;
		$newobj->_subject = $subject;
		
		return $newobj;
	}
	
}
